/* Poblando las tablas */
/* clientes */
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Juan Carlos', 'Anasicha', 'juan.anasicha@hotmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Nuria Elisa', 'Palacios', 'nuria.palacios@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Daniela Estefanía', 'López', 'daniela.lopez@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Jorge Rubén', 'Fernández', 'jorge.fernandez@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Marcia Raquel', 'Hernández', 'marcia.hernandez@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Gloria Eliana', 'Tovar', 'gloria.tovar@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Cesar Aníbal', 'Alvarez', 'cesar.alvarez@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Nina Lou', 'Saguay', 'nina.saguay@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Carolina de Lourdes', 'Paredes', 'carolina.paredes@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Jaime Santiago', 'Sandoval', 'jaime.sandoval@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Damián Patricio', 'Rojas', 'damian.rojas@gmail.com', '2022-09-26');
INSERT INTO clientes (nombre, apellido, email, create_at) VALUES('Mauricio Bayardo', 'Hurtado', 'mauricio.hurtado@gmail.com', '2022-09-26');

/* productos */
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Camiseta Capitán América', 19, NOW(), 50);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Camiseta Iron Man', 19, NOW(), 50);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Escudo Capitán América', 299, NOW(), 75);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Gafas para Niños Capitán América', 24, NOW(), 65);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Gafas para Niños Iron Man', 24, NOW(), 65);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Gorra para Niños, Capitán América', 14, NOW(), 42);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Gorra para niños Iron Man', 45, NOW(), 30);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Guantelete Electrónico', 24, NOW(), 10);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Guantelete del Infinito', 70, NOW(), 80);
INSERT INTO productos (nombre, precio, create_at, stock) VALUES('Monedero Capitán América', 35, NOW(), 100);

/* factura 01 */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura accesorios Marvel', null, 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 1);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 1, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 5);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(1, 1, 7);

/* factura 02 */
INSERT INTO facturas (descripcion, observacion, cliente_id, create_at) VALUES('Factura gorras marvel', 'Solamente gorras!', 1, NOW());
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(3, 2, 4);
INSERT INTO facturas_items (cantidad, factura_id, producto_id) VALUES(2, 2, 5);