package com.hulkstore.springboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase principal del proyecto springboot, en donde está la anotación @SpringBootApplication, esta clase ha sido generada automáticamente
 *
 * @author juan.anasicha
 * @changeLog 26 sep. 2022 - 09:38:47 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@SpringBootApplication
public class SpringBootDataJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootDataJpaApplication.class, args);
    }
}
