package com.hulkstore.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import com.hulkstore.springboot.app.modelo.entity.Cliente;
import com.hulkstore.springboot.app.modelo.service.IClienteService;
import com.hulkstore.springboot.app.util.Constantes.Etiquetas;
import com.hulkstore.springboot.app.util.Constantes.Rutas;
import com.hulkstore.springboot.app.util.paginator.PageRender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador en donde se gestionan los métodos que tienen que ver con el cliente
 *
 * @author juan.anasicha
 * @changeLog 27 sep. 2022 - 09:45:53 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Controller
@SessionAttributes("cliente")
public class ClienteController {
    @Autowired
    private IClienteService clienteService;

    /**
     * Método que permite mostrar en formato de formulario para poder ya sea crear o modificar un cliente
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 09:44:10 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param cliente
     * @param result
     * @param model
     * @param flash
     * @param status
     * @return
     */
    @RequestMapping(value = "/form")
    public String crear(Map<String, Object> model) {

        Cliente cliente = new Cliente();
        model.put(Etiquetas.ETIQUETA_CLIENTE, cliente);
        model.put(Etiquetas.ETIQUETA_TITULO, "Formulario de Cliente");
        return Rutas.Cliente.RUTA_CLIENTE_FORM;
    }

    /**
     * Método que permite buscar un cliente con la finalidad de constituirse en un insumo para poder mostrar dicha información para modificarla si es
     * el caso
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 09:46:24 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @param model
     * @param flash
     * @return
     */
    @RequestMapping(value = "/form/{id}")
    public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {

        Cliente cliente = null;
        if (id > 0) {
            cliente = clienteService.findOne(id);
            if (cliente == null) {
                flash.addFlashAttribute(Etiquetas.ETIQUETA_ERROR, "El ID del cliente no existe en la BBDD!");
                return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
            }
        } else {
            flash.addFlashAttribute(Etiquetas.ETIQUETA_ERROR, "El ID del cliente no puede ser cero!");
            return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
        }
        model.put(Etiquetas.ETIQUETA_CLIENTE, cliente);
        model.put(Etiquetas.ETIQUETA_TITULO, "Editar Cliente");
        return Rutas.Cliente.RUTA_CLIENTE_FORM;
    }

    /**
     * Método que permite eliminar el cliente dado un identificador
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 09:45:10 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @param flash
     * @return
     */
    @RequestMapping(value = "/eliminar/{id}")
    public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {

        if (id > 0) {
            clienteService.findOne(id);
            clienteService.delete(id);
            flash.addFlashAttribute("success", "Cliente eliminado con éxito!");
        }
        return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
    }

    /**
     * Método que permite guardar la información de un cliente en la base de datos, esta información puede ser nueva o a su vez modificada
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 09:44:10 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param cliente
     * @param result
     * @param model
     * @param flash
     * @param status
     * @return
     */
    @PostMapping("/form")
    public String guardar(@Valid Cliente cliente, BindingResult result, Model model, RedirectAttributes flash, SessionStatus status) {

        if (result.hasErrors()) {
            model.addAttribute(Etiquetas.ETIQUETA_TITULO, "Formulario de Cliente");
            return "form";
        }

        String mensajeFlash = (cliente.getId() != null) ? "Cliente editado con éxito!" : "Cliente creado con éxito!";

        clienteService.save(cliente);
        status.setComplete();
        flash.addFlashAttribute("success", mensajeFlash);
        return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
    }

    /**
     * Método que permite listar todos los clientes (empleados) registrados
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 09:42:20 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param page
     * @param model
     * @return
     */
    @GetMapping({ "/listar", "/" })
    public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {

        Pageable pageRequest = PageRequest.of(page, 4);

        Page<Cliente> clientes = clienteService.findAll(pageRequest);

        PageRender<Cliente> pageRender = new PageRender<Cliente>("/listar", clientes);
        model.addAttribute(Etiquetas.ETIQUETA_TITULO, "Listado de clientes (empleados)");
        model.addAttribute("clientes", clientes);
        model.addAttribute("page", pageRender);
        return Rutas.Cliente.RUTA_CLIENTE_LISTAR;
    }

    /**
     * Método que permite mostrar la información de un cliente con sus facturas asociadas
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 09:43:24 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @param model
     * @param flash
     * @return
     */
    @GetMapping(value = "/ver/{id}")
    public String ver(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {

        Cliente cliente = clienteService.fetchByIdWithFacturas(id);
        if (cliente == null) {
            flash.addFlashAttribute(Etiquetas.ETIQUETA_ERROR, "El cliente no existe en la base de datos");
            return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
        }

        model.put(Etiquetas.ETIQUETA_CLIENTE, cliente);
        model.put(Etiquetas.ETIQUETA_TITULO, "Detalle cliente: " + cliente.getNombre());
        return Rutas.Cliente.RUTA_CLIENTE_VER;
    }
}
