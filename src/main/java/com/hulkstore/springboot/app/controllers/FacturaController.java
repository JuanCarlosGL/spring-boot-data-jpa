package com.hulkstore.springboot.app.controllers;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import com.hulkstore.springboot.app.modelo.entity.Cliente;
import com.hulkstore.springboot.app.modelo.entity.Factura;
import com.hulkstore.springboot.app.modelo.entity.ItemFactura;
import com.hulkstore.springboot.app.modelo.entity.Producto;
import com.hulkstore.springboot.app.modelo.service.IClienteService;
import com.hulkstore.springboot.app.modelo.service.IFacturaService;
import com.hulkstore.springboot.app.modelo.service.IProductoService;
import com.hulkstore.springboot.app.util.Constantes.Etiquetas;
import com.hulkstore.springboot.app.util.Constantes.Rutas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador que permite gestionar los métodos que tienen que ver con las facturas
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 09:51:41 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Controller
@RequestMapping("/factura")
@SessionAttributes("factura")
public class FacturaController {
    @Autowired
    private IClienteService clienteService;

    @Autowired
    private IFacturaService fracturaService;

    @Autowired
    private IProductoService productoService;

    private final Logger log = LoggerFactory.getLogger(getClass());

    /**
     * Método que permite listar los productos en una factura asociada
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:03:51 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param term
     * @return
     */
    @GetMapping(value = "/cargar-productos/{term}", produces = { "application/json" })
    public @ResponseBody List<Producto> cargarProductos(@PathVariable String term) {
        return productoService.findByNombre(term);
    }

    /**
     * Método que permite buscar la factura dado su identificador, con la finalidad de que esta información se pueda constituir en un insumo para
     * poder mostrarla luego en un formulario para crear y/o editar
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:01:33 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param clienteId
     * @param model
     * @param flash
     * @return
     */
    @GetMapping("/form/{clienteId}")
    public String crear(@PathVariable(value = "clienteId") Long clienteId, Map<String, Object> model, RedirectAttributes flash) {

        Cliente cliente = clienteService.findOne(clienteId);

        if (cliente == null) {
            flash.addFlashAttribute(Etiquetas.ETIQUETA_ERROR, "El cliente no existe en la base de datos");
            return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
        }

        Factura factura = new Factura();
        factura.setCliente(cliente);

        model.put(Etiquetas.ETIQUETA_FACTURA, factura);
        model.put(Etiquetas.ETIQUETA_TITULO, Etiquetas.ETIQUETA_DESCRIPCION_TITULO_FACTURA);

        return Rutas.Factura.RUTA_FACTURA_FORM;
    }

    /**
     * Método que permite eliminar la factura dado un identificador, la factura se elimina con sus items asociados
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 09:56:00 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @param flash
     * @return
     */
    @GetMapping("/eliminar/{id}")
    public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash) {

        Factura factura = fracturaService.findFacturaById(id);

        if (factura != null) {
            fracturaService.deleteFactura(id);
            flash.addFlashAttribute("success", "Factura eliminada con éxito!");
            return "redirect:/ver/" + factura.getCliente().getId();
        }
        flash.addFlashAttribute(Etiquetas.ETIQUETA_ERROR, "La factura no existe en la base de datos, no se pudo eliminar!");

        return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
    }

    /**
     * Método que permite guardar la información concerniente a una factura y sus items, además permite disminuir las unidades en stock de conformidad
     * a los productos que se venden en dicha factura
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 09:54:26 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param factura
     * @param result
     * @param model
     * @param itemId
     * @param cantidad
     * @param flash
     * @param status
     * @return
     */
    @PostMapping("/form")
    public String guardar(@Valid Factura factura, BindingResult result, Model model, @RequestParam(name = "item_id[]", required = false) Long[] itemId,
            @RequestParam(name = "cantidad[]", required = false) Integer[] cantidad, RedirectAttributes flash, SessionStatus status) {

        if (result.hasErrors()) {
            model.addAttribute(Etiquetas.ETIQUETA_TITULO, Etiquetas.ETIQUETA_DESCRIPCION_TITULO_FACTURA);
            return Rutas.Factura.RUTA_FACTURA_FORM;
        }

        if ((itemId == null) || (itemId.length == 0)) {
            model.addAttribute(Etiquetas.ETIQUETA_TITULO, Etiquetas.ETIQUETA_DESCRIPCION_TITULO_FACTURA);
            model.addAttribute(Etiquetas.ETIQUETA_ERROR, "Error: La factura NO puede no tener líneas!");
            return Rutas.Factura.RUTA_FACTURA_FORM;
        }

        for (int i = 0; i < itemId.length; i++) {
            Producto producto = productoService.findOne(itemId[i]);
            if (producto.getStock() == 0) {
                model.addAttribute(Etiquetas.ETIQUETA_TITULO, Etiquetas.ETIQUETA_DESCRIPCION_TITULO_FACTURA);
                model.addAttribute(Etiquetas.ETIQUETA_ERROR, "Error: Por favor, revise el campo 'Disponibles en stock'!");
                return Rutas.Factura.RUTA_FACTURA_FORM;
            }
            producto.setStock(producto.getStock() - cantidad[i]);
            productoService.saveProducto(producto);

            ItemFactura linea = new ItemFactura();
            linea.setCantidad(cantidad[i]);
            linea.setProducto(producto);
            factura.addItemFactura(linea);
            log.info("ID: " + itemId[i].toString() + ", cantidad: " + cantidad[i].toString());
        }

        fracturaService.saveFactura(factura);
        status.setComplete();

        flash.addFlashAttribute("success", "Factura creada con éxito!");

        return "redirect:/ver/" + factura.getCliente().getId();
    }

    /**
     * Método que permite mostrar la información de una factura con sus respectivas entidades asociadas (items, clientes, productos)
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 09:52:27 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @param model
     * @param flash
     * @return
     */
    @GetMapping("/ver/{id}")
    public String ver(@PathVariable(value = "id") Long id, Model model, RedirectAttributes flash) {

        Factura factura = fracturaService.fetchFacturaByIdWithClienteWhithItemFacturaWithProducto(id);

        if (factura == null) {
            flash.addFlashAttribute(Etiquetas.ETIQUETA_ERROR, "La factura no existe en la base de datos!");
            return Rutas.Cliente.RUTA_CLIENTE_REDIRECT_LISTAR;
        }

        model.addAttribute(Etiquetas.ETIQUETA_FACTURA, factura);
        model.addAttribute(Etiquetas.ETIQUETA_TITULO, "Factura: ".concat(factura.getDescripcion()));

        return Rutas.Factura.RUTA_FACTURA_VER;
    }
}
