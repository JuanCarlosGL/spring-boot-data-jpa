package com.hulkstore.springboot.app.controllers;

import java.util.Map;

import javax.validation.Valid;

import com.hulkstore.springboot.app.modelo.entity.Producto;
import com.hulkstore.springboot.app.modelo.service.IProductoService;
import com.hulkstore.springboot.app.util.Constantes.Etiquetas;
import com.hulkstore.springboot.app.util.Constantes.Rutas;
import com.hulkstore.springboot.app.util.paginator.PageRender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Controlador para gestionar los métodos de los productos
 *
 * @author juan.anasicha
 * @changeLog 29 sep. 2022 - 10:05:08 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Controller
@RequestMapping("/producto")
@SessionAttributes("producto")
public class ProductoController {

    @Autowired
    private IProductoService productoService;

    /**
     * Método que permite redirigir al formulario del producto con la finalidad de crearlo de acuerdo a la información que ingrese el usuario
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 10:06:01 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param model
     * @return
     */
    @RequestMapping(value = "/form")
    public String crear(Map<String, Object> model) {

        Producto producto = new Producto();
        model.put(Etiquetas.ETIQUETA_PRODUCTO, producto);
        model.put(Etiquetas.ETIQUETA_TITULO, "Formulario de Producto");
        return Rutas.Producto.RUTA_PRODUCTO_FORM;
    }

    /**
     * Método que permite buscar el producto dado un identificador con la finalidad de constituirse en un insumo para mostrarlo en un formulario y
     * permitir que sea modificado
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 10:08:37 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @param model
     * @param flash
     * @return
     */
    @RequestMapping(value = "/form/{id}")
    public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash) {

        Producto producto = null;
        if (id > 0) {
            producto = productoService.findOne(id);
            if (producto == null) {
                flash.addFlashAttribute("error", "El ID del producto no existe en la BBDD!");
                return Rutas.Producto.RUTA_PRODUCTO_REDIRECT_LISTAR;
            }
        } else {
            flash.addFlashAttribute("error", "El ID del producto no puede ser cero!");
            return Rutas.Producto.RUTA_PRODUCTO_REDIRECT_LISTAR;
        }
        model.put(Etiquetas.ETIQUETA_PRODUCTO, producto);
        model.put(Etiquetas.ETIQUETA_TITULO, "Editar Producto");
        return Rutas.Producto.RUTA_PRODUCTO_FORM;
    }

    /**
     * Método que permite guardar la información de un producto, ya sea un nuevo registro o a su vez una actualización, generalmente del stock
     * disponible
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 10:09:49 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param producto
     * @param result
     * @param model
     * @param flash
     * @param status
     * @return
     */
    @PostMapping("/form")
    public String guardar(@Valid Producto producto, BindingResult result, Model model, RedirectAttributes flash, SessionStatus status) {

        if (result.hasErrors()) {
            model.addAttribute(Etiquetas.ETIQUETA_TITULO, "Formulario de Producto");
            return Rutas.Producto.RUTA_PRODUCTO_FORM;
        }

        String mensajeFlash = (producto.getId() != null) ? "Producto editado con éxito!" : "Producto creado con éxito!";
        productoService.saveProducto(producto);
        status.setComplete();
        flash.addFlashAttribute("success", mensajeFlash);
        return Rutas.Producto.RUTA_PRODUCTO_REDIRECT_LISTAR;
    }

    /**
     * Método que permite listar los productos de la tienda Hulk Store
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 10:10:32 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param page
     * @param model
     * @return
     */
    @GetMapping("/listar")
    public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
        Pageable pageRequest = PageRequest.of(page, 4);
        Page<Producto> productos = productoService.findAll(pageRequest);
        PageRender<Producto> pageRender = new PageRender<>("/producto/listar", productos);
        model.addAttribute(Etiquetas.ETIQUETA_TITULO, "Listado de productos");
        model.addAttribute("productos", productos);
        model.addAttribute("page", pageRender);
        return Rutas.Producto.RUTA_PRODUCTO_LISTAR;
    }
}
