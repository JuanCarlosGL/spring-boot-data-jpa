package com.hulkstore.springboot.app.modelo.service.impl;

import java.util.List;

import com.hulkstore.springboot.app.modelo.dao.IProductoDao;
import com.hulkstore.springboot.app.modelo.entity.Producto;
import com.hulkstore.springboot.app.modelo.service.IProductoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Clase en donde se implementan los servicios definidos en la interfaz producto
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 10:44:00 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Service
public class ProductoServiceImpl implements IProductoService {

    @Autowired
    private IProductoDao productoDao;

    @Override
    @Transactional(readOnly = true)
    public Page<Producto> findAll(Pageable pageable) {
        return productoDao.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Producto> findByNombre(String term) {
        return productoDao.findByNombreLikeIgnoreCase("%" + term + "%");
    }

    @Override
    @Transactional(readOnly = true)
    public Producto findOne(Long id) {
        // TODO Auto-generated method stub
        return productoDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void saveProducto(Producto producto) {
        productoDao.save(producto);
    }

}
