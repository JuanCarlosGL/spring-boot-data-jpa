package com.hulkstore.springboot.app.modelo.dao;

import com.hulkstore.springboot.app.modelo.entity.Cliente;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Interfaz ClienteDao la cual hereda de la interfaz PagingAndSortingRepository con la finalidad de usar los métodos de ordenación y paginación que
 * sean
 * necesarios
 *
 * @author juan.anasicha
 * @changeLog 27 sep. 2022 - 10:18:49 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public interface IClienteDao extends PagingAndSortingRepository<Cliente, Long> {

    /**
     * Método que permite devolver un cliente con su factura asociada
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 10:28:00 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    @Query("select c from Cliente c left join fetch c.facturas f where c.id=?1")
    public Cliente fetchByIdWithFacturas(Long id);

}
