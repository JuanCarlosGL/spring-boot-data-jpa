package com.hulkstore.springboot.app.modelo.dao;

import java.util.List;

import com.hulkstore.springboot.app.modelo.entity.Producto;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Interfaz productoDao la cual hereda de la interfaz PagingAndSortingRepository PagingAndSortingRepository con la finalidad de usar los métodos de
 * ordenación y paginación que sean necesarios
 *
 * @author juan.anasicha
 * @changeLog 27 sep. 2022 - 10:23:34 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public interface IProductoDao extends PagingAndSortingRepository<Producto, Long> {

    /**
     * Método que permite buscar un listado de productos por nombre (like)
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 10:25:31 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param term
     * @return
     */
    @Query("select p from Producto p where p.nombre like %?1%")
    public List<Producto> findByNombre(String term);

    /**
     * Método que permite buscar un listado de productos por nombre (like Ignore Case)
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 10:24:54 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param term
     * @return
     */
    public List<Producto> findByNombreLikeIgnoreCase(String term);

}
