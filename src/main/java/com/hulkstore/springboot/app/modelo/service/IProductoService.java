package com.hulkstore.springboot.app.modelo.service;

import java.util.List;

import com.hulkstore.springboot.app.modelo.entity.Producto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Interfaz producto la cual permite definir los servicios que gestionan el comportamient de los productos
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 10:31:44 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public interface IProductoService {

    /**
     * Servicio que permite devolver todos los productos para mostrarlos de forma paginada
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:34:28 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param pageable
     * @return
     */
    public Page<Producto> findAll(Pageable pageable);

    /**
     * Servicio que permite buscar un listado de productos dado el nombre (like)
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:33:50 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param term
     * @return
     */
    public List<Producto> findByNombre(String term);

    /**
     * Servicio que permite buscar un producto dado el identificador
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:33:16 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    public Producto findOne(Long id);

    /**
     * Servicio que permite guardar la información de un producto
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:32:35 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param producto
     */
    public void saveProducto(Producto producto);

}
