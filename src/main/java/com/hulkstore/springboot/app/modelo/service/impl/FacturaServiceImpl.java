package com.hulkstore.springboot.app.modelo.service.impl;

import com.hulkstore.springboot.app.modelo.dao.IFacturaDao;
import com.hulkstore.springboot.app.modelo.entity.Factura;
import com.hulkstore.springboot.app.modelo.service.IFacturaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Clase en donde se implementan los servicios definidos en la interfaz factura
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 10:43:55 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Service
public class FacturaServiceImpl implements IFacturaService {

    @Autowired
    private IFacturaDao facturaDao;

    @Override
    @Transactional
    public void deleteFactura(Long id) {
        facturaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Factura fetchFacturaByIdWithClienteWhithItemFacturaWithProducto(Long id) {
        return facturaDao.fetchByIdWithClienteWhithItemFacturaWithProducto(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Factura findFacturaById(Long id) {
        // TODO Auto-generated method stub
        return facturaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void saveFactura(Factura factura) {
        facturaDao.save(factura);
    }
}
