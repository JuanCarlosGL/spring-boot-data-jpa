package com.hulkstore.springboot.app.modelo.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

/**
 * Entidad factura la cual representa la estructura de la base de datos en donde se guarda la información cabecera de las facturas por las ventas de
 * los productos a un cliente / empleado
 *
 * @author juan.anasicha
 * @changeLog 26 sep. 2022 - 10:13:03 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Entity
@Table(name = "facturas")
public class Factura {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String descripcion;

    private String observacion;

    @Temporal(TemporalType.DATE)
    @Column(name = "create_at")
    private Date createAt;

    @ManyToOne(fetch = FetchType.LAZY)
    private Cliente cliente;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "factura_id")
    private List<ItemFactura> items;

    public Factura() {
        items = new ArrayList<ItemFactura>();
    }

    public void addItemFactura(ItemFactura item) {
        items.add(item);
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Long getId() {
        return id;
    }

    public List<ItemFactura> getItems() {
        return items;
    }

    public String getObservacion() {
        return observacion;
    }

    public Double getTotal() {
        Double total = 0.0;

        int size = items.size();

        for (int i = 0; i < size; i++) {
            total += items.get(i).calcularImporte();
        }
        return total;
    }

    @PrePersist
    public void prePersist() {
        createAt = new Date();
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setItems(List<ItemFactura> items) {
        this.items = items;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
