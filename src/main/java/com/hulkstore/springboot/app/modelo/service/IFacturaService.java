package com.hulkstore.springboot.app.modelo.service;

import com.hulkstore.springboot.app.modelo.entity.Factura;

/**
 * Interfaz factura la cual permite definir los servicios que gestionan el comportamiento para las facturas y sus items asociados de hulk store
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 10:30:19 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public interface IFacturaService {

    /**
     * Servicio que permite eliminar una factura y sus items asociados, dado el identificador de la factura.
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:37:58 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     */
    public void deleteFactura(Long id);

    /**
     * Servicio que permite buscar una factura dado el identificador, además encuentravsu respectivo cliente, producto, e items asociados.
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:36:46 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    public Factura fetchFacturaByIdWithClienteWhithItemFacturaWithProducto(Long id);

    /**
     * Servicio que permite buscar una factura y sus items dado el identificador
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:36:11 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    public Factura findFacturaById(Long id);

    /**
     * Servicio que permite guardar la información de una factura y sus items asociados
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:35:27 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param factura
     */
    public void saveFactura(Factura factura);

}
