package com.hulkstore.springboot.app.modelo.dao;

import com.hulkstore.springboot.app.modelo.entity.Factura;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Interfaz facturaDao la cual hereda de la interfaz CrudRepository con la finalidad de poder usar los métodos básicos para CRUD
 *
 * @author juan.anasicha
 * @changeLog 27 sep. 2022 - 10:22:02 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public interface IFacturaDao extends CrudRepository<Factura, Long> {

    /**
     * Método que permite devolver una factura con su cliente e items asociados
     *
     * @author juan.anasicha
     * @changeLog 27 sep. 2022 - 10:27:13 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    @Query("select f from Factura f join fetch f.cliente c join fetch f.items l join fetch l.producto where f.id=?1")
    public Factura fetchByIdWithClienteWhithItemFacturaWithProducto(Long id);

}
