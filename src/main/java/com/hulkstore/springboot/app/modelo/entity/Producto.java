package com.hulkstore.springboot.app.modelo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * Entidad producto la cual representa la estructura de la base de datos encargada de almacenar la información de los productos y su respectivo stock
 * disponible, y adicionalmente se agrega un precio para permitir realizar cálculos
 *
 * @author juan.anasicha
 * @changeLog 26 sep. 2022 - 10:16:48 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@Entity
@Table(name = "productos")
public class Producto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String nombre;

    @NotNull
    @Min(value = 1)
    @Digits(fraction = 2, integer = 3)
    private Double precio;

    @NotNull
    @Min(value = 0)
    @Digits(fraction = 0, integer = 2)
    private Integer stock;

    @NotNull
    @Column(name = "create_at")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createAt;

    public Date getCreateAt() {
        return createAt;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public Integer getStock() {
        return stock;
    }

    @PrePersist
    public void prePersist() {
        createAt = new Date();
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }
}
