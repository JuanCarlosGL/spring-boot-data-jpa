package com.hulkstore.springboot.app.modelo.service;

import java.util.List;

import com.hulkstore.springboot.app.modelo.entity.Cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Interfaz cliente la cual permite definir los servicios que gestionan el comportamiento del cliente / empleado en hulk store
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 10:29:24 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public interface IClienteService {

    /**
     * Servicio que permite eliminar un cliente dado el identificador
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:42:50 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     */
    public void delete(Long id);

    /**
     * Servicio que permite buscar un cliente con sus facturas asociadas
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:42:27 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    public Cliente fetchByIdWithFacturas(Long id);

    /**
     * Servicio que permite listar los clientes de Hulk Store
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:41:52 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @return
     */
    public List<Cliente> findAll();

    /**
     * Servicio que permite devolver todos los clientes para poder mostrarlos de forma paginada
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:41:20 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param pageable
     * @return
     */
    public Page<Cliente> findAll(Pageable pageable);

    /**
     * Servicio que permite buscar un cliente dado el identificador
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:40:50 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param id
     * @return
     */
    public Cliente findOne(Long id);

    /**
     * Servicio que permite guardar la información de un cliente
     *
     * @author juan.anasicha
     * @changeLog 28 sep. 2022 - 10:40:25 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     * @param cliente
     */
    public void save(Cliente cliente);
}
