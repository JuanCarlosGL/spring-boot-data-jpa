package com.hulkstore.springboot.app.util;

/**
 * Clase en donde se definen las constantes que se utilizan en el proyecto Huñlk Store
 *
 * @author juan.anasicha
 * @changeLog 28 sep. 2022 - 10:45:41 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public class Constantes {
    public static final class Etiquetas {
        public static final String ETIQUETA_DESCRIPCION_TITULO_FACTURA = "Crear Factura";
        public static final String ETIQUETA_CLIENTE = "cliente";
        public static final String ETIQUETA_ERROR = "error";
        public static final String ETIQUETA_FACTURA = "factura";
        public static final String ETIQUETA_PRODUCTO = "producto";
        public static final String ETIQUETA_TITULO = "titulo";
    }

    public static final class Rutas {
        public static final class Cliente {
            public static final String RUTA_CLIENTE_REDIRECT_LISTAR = "redirect:/listar";
            public static final String RUTA_CLIENTE_FORM = "form";
            public static final String RUTA_CLIENTE_LISTAR = "listar";
            public static final String RUTA_CLIENTE_VER = "ver";
        }

        public static final class Factura {
            public static final String RUTA_FACTURA_FORM = "factura/form";
            public static final String RUTA_FACTURA_VER = "factura/ver";
        }

        public static final class Producto {
            public static final String RUTA_PRODUCTO_FORM = "producto/form";
            public static final String RUTA_PRODUCTO_LISTAR = "producto/listar";
            public static final String RUTA_PRODUCTO_REDIRECT_LISTAR = "redirect:/producto/listar";
        }
    }
}
