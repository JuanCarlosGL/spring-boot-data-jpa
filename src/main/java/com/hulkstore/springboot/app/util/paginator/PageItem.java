package com.hulkstore.springboot.app.util.paginator;

/**
 * Clase para definir los atributos de una página, la cual se usa en la clase PageRender
 *
 * @author juan.anasicha
 * @changeLog 27 sep. 2022 - 14:46:37 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
public class PageItem {

    private int numero;
    private boolean actual;

    public PageItem(int numero, boolean actual) {
        this.numero = numero;
        this.actual = actual;
    }

    public int getNumero() {
        return numero;
    }

    public boolean isActual() {
        return actual;
    }
}
