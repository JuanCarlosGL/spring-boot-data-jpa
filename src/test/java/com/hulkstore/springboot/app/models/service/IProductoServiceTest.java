package com.hulkstore.springboot.app.models.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import com.hulkstore.springboot.app.modelo.entity.Producto;
import com.hulkstore.springboot.app.modelo.service.IProductoService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Clase para testear los servicios definidos en la interfaz producto
 *
 * @author juan.anasicha
 * @changeLog 29 sep. 2022 - 11:09:00 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@SpringBootTest
class IProductoServiceTest {

    @Autowired
    IProductoService productoService;

    /**
     * Método que permite testear el servico 'findByNombre', además se testea implícitamente el servicio 'saveProducto'
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 11:09:24 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     */
    @Test
    void findByNombre() {
        Producto producto = new Producto();
        producto.setNombre("Producto Test 1");
        producto.setPrecio(100.00);
        producto.setStock(99);
        productoService.saveProducto(producto);
        Boolean actualResult = false;
        List<Producto> productos = productoService.findByNombre("Producto Test 1");
        for (Producto oProducto : productos) {
            if (oProducto.getNombre().equalsIgnoreCase("Producto Test 1")) {
                actualResult = true;
            }
        }
        assertThat(actualResult).isTrue();
    }

    /**
     * Método que permite testear el servicio 'findOne', además implicitamente se testea el servicio 'saveProducto'
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 11:10:35 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     */
    @Test
    void findOne() {
        Producto producto = new Producto();
        producto.setNombre("Producto Test 2");
        producto.setPrecio(200.00);
        producto.setStock(88);
        productoService.saveProducto(producto);
        Boolean actualResult = false;
        if (productoService.findOne(producto.getId()) != null) {
            actualResult = true;
        }
        assertThat(actualResult).isTrue();
    }

    /**
     * Método que permite testear el servicio 'saveProducto'
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 11:11:20 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     */
    @Test
    void saveProducto() {
        Producto producto = new Producto();
        producto.setNombre("Producto Test 3");
        producto.setPrecio(300.00);
        producto.setStock(77);
        productoService.saveProducto(producto);
        Boolean actualResult = false;
        if (producto.getId() != null) {
            actualResult = true;
        }
        assertThat(actualResult).isTrue();
    }
}
