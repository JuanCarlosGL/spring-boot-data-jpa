package com.hulkstore.springboot.app.models.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import com.hulkstore.springboot.app.modelo.entity.Factura;
import com.hulkstore.springboot.app.modelo.service.IFacturaService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Clases para testear los servicios definidos en la interfaz factura
 *
 * @author juan.anasicha
 * @changeLog 29 sep. 2022 - 11:25:02 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@SpringBootTest
class IFacturaServiceTest {

    @Autowired
    IFacturaService facturaService;

    /**
     * Método que permite testear el servicio 'delete', implícitamente se prueba el método 'findFacturaById', y el método 'saveFactura' los cuales son
     * los más relevantes de dicha interfaz
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 11:32:59 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     */
    @Test
    void delete() {
        Factura factura = new Factura();
        factura.setDescripcion("Descripción Factura Test 1");
        factura.setCreateAt(new Date());
        facturaService.saveFactura(factura);
        Boolean actualResult = false;
        if (facturaService.findFacturaById(factura.getId()) != null) {
            facturaService.deleteFactura(factura.getId());
            if (facturaService.findFacturaById(factura.getId()) == null) {
                actualResult = true;
            }
        }
        assertThat(actualResult).isTrue();
    }
}
