package com.hulkstore.springboot.app.models.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import com.hulkstore.springboot.app.modelo.entity.Cliente;
import com.hulkstore.springboot.app.modelo.service.IClienteService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Clase para testear los servicios definidos en la interfaz cliente
 *
 * @author juan.anasicha
 * @changeLog 29 sep. 2022 - 11:06:47 juan.anasicha
 *            Emisión inicial.
 *            .. Más entradas de los cambios aquí....
 */
@SpringBootTest
class IClienteServiceTest {

    @Autowired
    IClienteService clienteService;

    /**
     * Método que permite testear el servicio 'delete'
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 11:29:09 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     */
    @Test
    void delete() {
        Cliente cliente = new Cliente();
        cliente.setNombre("Nombre Cliente Test 2");
        cliente.setApellido("Apellido Cliente Test 2");
        cliente.setEmail("emai.clientel@test2.com");
        cliente.setCreateAt(new Date());
        clienteService.save(cliente);
        Boolean actualResult = false;
        if (clienteService.findOne(cliente.getId()) != null) {
            clienteService.delete(cliente.getId());
            if (clienteService.findOne(cliente.getId()) == null) {
                actualResult = true;
            }
        }
        assertThat(actualResult).isTrue();
    }

    /**
     * Método que permite testear los servicios 'findOne', y además implícitamente se testea el método 'save' son los más relevantes de dicha interfaz
     *
     * @author juan.anasicha
     * @changeLog 29 sep. 2022 - 11:07:25 juan.anasicha
     *            Emisión inicial.
     *            .. Más entradas de los cambios aquí....
     */
    @Test
    void findOne() {
        Cliente cliente = new Cliente();
        cliente.setNombre("Nombre Cliente Test 1");
        cliente.setApellido("Apellido Cliente Test 1");
        cliente.setEmail("emai.clientel@test1.com");
        cliente.setCreateAt(new Date());
        clienteService.save(cliente);
        Boolean actualResult = false;
        if (clienteService.findOne(cliente.getId()) != null) {
            actualResult = true;
        }
        assertThat(actualResult).isTrue();
    }
}
